<?php

namespace App\Http\Controllers;

use App\Commands\Products\CreateNewProduct;
use App\Commands\SimpleCommandBus;
use Illuminate\Http\Request;

/**
 * Class ProductsController related for operations for products
 * @package App\Http\Controllers
 */
class ProductsController extends Controller
{

    /**
     * Store new product
     *
     * @param Request $request
     * @param SimpleCommandBus $bus
     * @return array
     */
    public function store(Request $request, SimpleCommandBus $bus)
    {
        $name = $request->post('name');
        $price = (float)$request->post('price');

        if(!empty($name) && $price > 0){
            $bus->handle(new CreateNewProduct($name, $price));
        } else {
            abort(500);
        }
        return ['message' => 'Product was created.'];
    }

}
