<?php


namespace App\Domain\Products;

/**
 * Interface ProductsRepositoryInterface
 * @package App\Domain\Products
 */
interface ProductsRepositoryInterface
{
    /**
     * Add product
     * @param Product $product
     */
    public function add(Product $product): void;
}
