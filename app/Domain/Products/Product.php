<?php


namespace App\Domain\Products;

use Ramsey\Uuid\Uuid;

/**
 * Class Product
 * @package App\Domain\Products
 */
final class Product
{
    /**
     * @var int UUID
     */
    private $id;

    /**
     * @var string Name of product
     */
    private $name;

    /**
     * @var float Price of product
     */
    private $price;

    /**
     * Product constructor.
     * @param string $name
     * @param float $price
     */
    public function __construct(string $name, float $price)
    {
        $this->id = Uuid::uuid1();
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
}
