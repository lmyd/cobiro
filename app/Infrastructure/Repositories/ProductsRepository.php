<?php


namespace App\Infrastructure\Repositories;


use App\Domain\Products\Product;
use App\Domain\Products\ProductsRepositoryInterface;
use App\Models\Product as ProductModel;

/**
 * Class ProductsRepository
 * @package App\Infrastructure\Repositories
 */
class ProductsRepository implements ProductsRepositoryInterface
{

    /**
     * Add product
     * @param Product $product
     */
    public function add(Product $product): void
    {
        ProductModel::create([
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice()
        ]);
    }
}
