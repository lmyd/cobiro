<?php

namespace App\Providers;

use App\Commands\Products\CreateNewProduct;
use App\Commands\Products\CreateNewProductHandler;
use App\Commands\SimpleCommandBus;
use App\Infrastructure\Repositories\ProductsRepository;
use Illuminate\Support\ServiceProvider;

class CqrsCommandsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SimpleCommandBus::class, function ($app) {
            $bus = new SimpleCommandBus();
            $bus->registerHandler(CreateNewProduct::class, new CreateNewProductHandler(new ProductsRepository()));
            return $bus;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
