<?php


namespace App\Commands;

/**
 * Interface CommandBusInterface
 * @package App\Commands
 */
interface CommandBusInterface
{
    /**
     * Register command with command handler
     * @param string $commandClass
     * @param CommandHandlerInterface $handler
     */
    public function registerHandler(string $commandClass, CommandHandlerInterface $handler): void;

    /**
     * Execute command
     * @param CommandInterface $command
     */
    public function handle(CommandInterface $command): void;
}
