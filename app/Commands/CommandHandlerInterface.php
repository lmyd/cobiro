<?php


namespace App\Commands;

/**
 * Interface CommandHandlerInterface
 * @package App\Commands
 */
interface CommandHandlerInterface
{
    /**
     * Execute command
     * @param CommandInterface $command
     */
    public function handle(CommandInterface $command): void;
}
