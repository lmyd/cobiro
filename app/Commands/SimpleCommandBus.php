<?php


namespace App\Commands;

use mysql_xdevapi\Exception;

/**
 * Class SimpleCommandBus
 * @package App\Commands
 */
class SimpleCommandBus implements CommandBusInterface
{
    /**
     * @var array registered handlers
     */
    private $handlers = [];

    /**
     * Register command with command handler
     * @param string $commandClass
     * @param CommandHandlerInterface $handler
     */
    public function registerHandler(string $commandClass, CommandHandlerInterface $handler): void
    {
        $this->handlers[$commandClass] = $handler;
    }

    /**
     * Execute command
     * @param CommandInterface $command
     */
    public function handle(CommandInterface $command): void
    {
        $commandClass = get_class($command);

        if(array_key_exists($commandClass, $this->handlers))
            $this->handlers[$commandClass]->handle($command);
        else
            throw new Exception('Command handler related with command '.$commandClass.' not registered.');
    }
}
