<?php


namespace App\Commands\Products;

use App\Commands\CommandHandlerInterface;
use App\Commands\CommandInterface;
use App\Domain\Products\Product;
use App\Domain\Products\ProductsRepositoryInterface;

/**
 * Class CreateNewProductHandler
 * @package App\Commands\Products
 */
class CreateNewProductHandler implements CommandHandlerInterface
{
    /**
     * @var ProductsRepositoryInterface
     */
    private $productsRepository;

    /**
     * CreateNewProductHandler constructor.
     * @param ProductsRepositoryInterface $productsRepository
     */
    public function __construct(ProductsRepositoryInterface $productsRepository)
    {
        $this->productsRepository = $productsRepository;
    }

    /**
     * Execute command
     * @param CreateNewProduct $command
     */
    public function handle(CommandInterface $command): void
    {
        $product = new Product(
            $command->getName(),
            $command->getPrice()
        );
        $this->productsRepository->add($product);
    }
}
