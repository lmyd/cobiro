<?php


namespace App\Commands\Products;


use App\Commands\CommandInterface;

/**
 * Class CreateNewProduct
 * @package App\Commands\Products
 */
class CreateNewProduct implements CommandInterface
{
    /**
     * @var string Product name
     */
    private $name;

    /**
     * @var float Product price
     */
    private $price;

    /**
     * CreateNewProduct constructor.
     * @param string $name
     * @param float $price
     */
    public function __construct(string $name, float $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
}
